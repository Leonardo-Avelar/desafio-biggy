from bateria import Bateria

class Lanterna():

    def __init__(self, bateria):
        self.__status = 'Desligada'
        self.__bateria = bateria
    
    def getStatus(self):
        return self.__status
    
    def getBateria(self):
        return self.__bateria
    
    def interruptor(self):

        if( self.__status == 'Desligada' and self.__bateria.getCarga() > 0 ):
            self.__status = 'Ligada'
        
        else:
            self.__status = 'Desligada'
    
    def trocarBateria(self):

        self.__bateria = Bateria(100)