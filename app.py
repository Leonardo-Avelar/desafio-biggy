# -*- coding:utf-8 -*-

import threading
from tkinter import *

from bateria import Bateria
from lanterna import Lanterna


class Interface(threading.Thread):

    def __init__(self, toplevel, lanterna):

        threading.Thread.__init__(self)
        self.__event = threading.Event()

        toplevel.title('Lanterna')
        toplevel.geometry("350x300")
        self.__frame1 = Frame(toplevel, pady = 30)
        self.__frame2 = Frame(toplevel, pady = 5)
        self.__frame3 = Frame(toplevel)
        self.__frame4 = Frame(toplevel, pady = 10)
        self.__frame5 = Frame(toplevel)
        self.__frame6 = Frame(toplevel, pady = 10)
        self.__frame1.pack()
        self.__frame2.pack()
        self.__frame3.pack()
        self.__frame4.pack()
        self.__frame5.pack()
        self.__frame6.pack()
        
        # Titulo
        Label(self.__frame1, text = 'Lanterna em Python', fg = 'black', font = ('Arial, Verdana', '15'), height = 1).pack()
        fonte = ('Arial, Verdana', '10')

        # Label 1 para exibir status da lanterna
        Label(self.__frame2, text = 'Status: ', font = fonte, width = 8).pack(side = LEFT)
        self.__labelStatus = Label(self.__frame2, width = 10, font = fonte)
        self.__labelStatus['text'] = lanterna.getStatus()        
        self.__labelStatus.pack(side = LEFT)

        # Label 2 para exibir status da bateria
        Label(self.__frame3, text = 'Bateria :', font = fonte, width = 8).pack(side = LEFT)
        self.__labelBateria = Label(self.__frame3, width = 10, font = fonte)
        self.__labelBateria['text'] = str( lanterna.getBateria().getCarga() ) + '%'
        self.__labelBateria.pack(side = LEFT)

        # Botao Liga/Desliga
        interruptor = Button(self.__frame4, font = fonte, width = 20, text = 'Liga/Desliga', command = self.interruptor)
        interruptor.pack(side = LEFT)

        # Botao Trocar Baterria
        bateria = Button(self.__frame5, font = fonte, width = 20, text = 'Trocar baterria', command = self.trocarBateria)
        bateria.pack(side = LEFT)

        # Botao Sair
        sair = Button(self.__frame6, font = fonte, width = 20, text = 'Sair', command = self.sair)
        sair.pack(side = LEFT)

        # Outros atributos
        self.__lanterna = lanterna
        self.__loop = False
    
    def interruptor(self):
    
        lanterna = self.__lanterna
        bateria = lanterna.getBateria()
        cargaBateria = bateria.getCarga()
        
        if cargaBateria > 0 or cargaBateria <= 0 and lanterna.getStatus() == 'Ligada':
            lanterna.interruptor()
            self.__labelStatus['text'] = lanterna.getStatus()

        if self.__loop == False:
            self.__loop = True
            self.start()
    
    def trocarBateria(self):
        
        lanterna = self.__lanterna
        lanterna.trocarBateria()

        bateria = lanterna.getBateria()
        self.__labelBateria['text'] = str( bateria.getCarga() ) + '%'

    def sair(self):

        self.__loop = False
        self.stop()
        tk.destroy()
    
    def run(self):
        
        while(self.__loop == True):

            while self.__lanterna.getBateria().getCarga() >= 0 and self.__lanterna.getStatus() == 'Ligada' and not self.__event.is_set():
                self.__labelBateria['text'] = str( self.__lanterna.getBateria().getCarga() ) + '%'
                self.__lanterna.getBateria().diminuirCarga()
                self.__event.wait(1)
            
            if self.__lanterna.getBateria().getCarga() <= 0 and self.__lanterna.getStatus() == 'Ligada':
                self.interruptor()
    
    def stop(self):
        self.__event.set()

if __name__ == '__main__':

    try:
        bateria = Bateria(100)
        lanterna = Lanterna(bateria)
        tk = Tk()
        interface = Interface(tk, lanterna)

        tk.mainloop()
    
    except:
        print("Erro na instanciacao de objetos")