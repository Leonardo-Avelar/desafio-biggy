class Bateria():

    def __init__(self, carga):
        self.__carga = carga
    
    def getCarga(self):
        return self.__carga
    
    def diminuirCarga(self):
        self.__carga -= 1